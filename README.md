# Spring Boot

This is a sample Java / Maven / Spring Boot application that can be used as a starter for creating a microservice complete. This project use Oauth2 Authentication and Postman export can be found on file ProvaM.postman_collection.json

This project use H2 memory database.

Endpoints:
Client API
  - [POST] http://localhost:8088/api/oauth/token
  - [GET] http://localhost:8088/api/api/interview/client
  - [GET] http://localhost:8088/api/api/interview/client/{Id}
  - [PUT] http://localhost:8088/api/api/interview/client/{Id}
  - [DELETE] http://localhost:8088/api/api/interview/client/{Id}

Product API
  - [GET] http://localhost:8088/api/interview/product/?page=1

### Installation

Install the dependencies and devDependencies .

```sh
$ mvn clean install
```

### Sonar 
To run sonnar you must alter sonar-project.properties and run.
```sh
$ sonar-scanner
```


### Dockergit sta 
```sh
docker build --pull --rm -f "Dockerfile" -t interviewm:latest "."
docker run -d --name interview  -p 8088:8088 interviewm
```
