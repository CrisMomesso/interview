package com.rest.api.interview.repository;

import com.rest.api.interview.model.Product;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * IProductRepository
 */
public interface IProductRepository extends JpaRepository<Product, Long> {

}