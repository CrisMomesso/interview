package com.rest.api.interview.repository;

import java.util.Optional;

import com.rest.api.interview.model.Client;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * IClientReposotory
 */
public interface IClientRepository extends JpaRepository<Client, Long> {
    public Optional<Client> findByName(String name);

    public Optional<Client> findByEmail(String email);
}