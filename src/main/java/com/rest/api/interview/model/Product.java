package com.rest.api.interview.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @NotBlank
    private double price;
    @NotNull
    @NotBlank
    private String image;
    @NotNull
    @NotBlank
    private String brand;
    @NotNull
    @NotBlank
    private String title;
    @NotNull
    @NotBlank
    @Column(name = "REVIEWSCORE")
    private int reviewScore;
}
