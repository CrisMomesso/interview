package com.rest.api.interview.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.rest.api.interview.dto.ClientDTO;
import com.rest.api.interview.exception.ConstraintsViolationException;
import com.rest.api.interview.exception.EntityNotFoundException;
import com.rest.api.interview.exception.InternalServerErrorExeption;
import com.rest.api.interview.service.IClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/client")
public class ClientController {

    @Autowired
    private IClientService clientService;

    
    /** 
     * @return List<ClientDTO>
     * @throws InternalServerErrorExeption
     */
    @GetMapping
    @ApiOperation(value = "List All Clients")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<ClientDTO> getAll() throws InternalServerErrorExeption {
        try {
            return clientService.get(1,100).stream().collect(Collectors.toList());
        } catch (Exception e) {
            throw new InternalServerErrorExeption("internal error");
        }
    }

    
    /** 
     * @param name
     * @return ClientDTO
     * @throws EntityNotFoundException
     */
    @GetMapping("/name/{name}")
    @ApiOperation(value = "List client by name")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ClientDTO get(@PathVariable String name) throws EntityNotFoundException {
        return clientService.findByName(name);
    }

    
    /** 
     * @param clientDto
     * @return ClientDTO
     * @throws ConstraintsViolationException
     */
    @PostMapping
    @ApiOperation(value = "Insert client")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ClientDTO create(@RequestBody ClientDTO clientDto) throws ConstraintsViolationException {
        return clientService.create(clientDto);
    }

    
    /** 
     * @param id
     * @param clientDto
     * @return ClientDTO
     * @throws ConstraintsViolationException
     * @throws EntityNotFoundException
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "Update client")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ClientDTO alter(@PathVariable long id, @RequestBody ClientDTO clientDto)
            throws ConstraintsViolationException, EntityNotFoundException {
        return clientService.alter(id, clientDto);
    }

    
    /** 
     * @param id
     * @return ClientDTO
     * @throws EntityNotFoundException
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "Find client by id")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ClientDTO get(@PathVariable Long id) throws EntityNotFoundException {
        return clientService.findById(id);
    }

    
    /** 
     * @param id
     * @throws InternalServerErrorExeption
     */
    @ResponseBody
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Long id) throws InternalServerErrorExeption {
        clientService.deleteById(id);
    }

}