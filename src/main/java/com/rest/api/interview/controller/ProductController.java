package com.rest.api.interview.controller;

import java.util.Set;
import java.util.stream.Collectors;

import com.rest.api.interview.dto.MetaDTO;
import com.rest.api.interview.dto.ProductDTO;
import com.rest.api.interview.dto.ProductResponseDTO;
import com.rest.api.interview.exception.InternalServerErrorExeption;
import com.rest.api.interview.service.IProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    private IProductService productService;

    
    /** 
     * @param "page"
     * @param false
     * @param page
     * @return ProductResponseDTO
     * @throws InternalServerErrorExeption
     */
    @GetMapping
    @ApiOperation(value = "Get products")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ProductResponseDTO get(@RequestParam(value = "page", required = false, defaultValue = "0") int page)
            throws InternalServerErrorExeption {
        try {
            Set<ProductDTO> products = productService.get(page, 10).stream().collect(Collectors.toSet());
            MetaDTO meta = new MetaDTO(page, 10);
            ProductResponseDTO response = new ProductResponseDTO(meta, products);
            return response;
        } catch (Exception e) {
            throw new InternalServerErrorExeption("internal error");
        }
    }

}
