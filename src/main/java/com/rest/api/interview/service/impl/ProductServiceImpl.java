package com.rest.api.interview.service.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.rest.api.interview.dto.ProductDTO;
import com.rest.api.interview.exception.ConstraintsViolationException;
import com.rest.api.interview.exception.EntityNotFoundException;
import com.rest.api.interview.exception.InternalServerErrorExeption;
import com.rest.api.interview.mapper.ProductMapper;
import com.rest.api.interview.model.Product;
import com.rest.api.interview.repository.IProductRepository;
import com.rest.api.interview.service.IProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements IProductService {

    private IProductRepository prodRepo;

    @Autowired
    public ProductServiceImpl(IProductRepository prodRepo) {
        this.prodRepo = prodRepo;
    }

    @Override
    public Set<ProductDTO> get(int page, int size) {
        Pageable paging = PageRequest.of(page, size);
        Page<Product> result = prodRepo.findAll(paging);
        return ProductMapper.makeDTOList(result.get().collect(Collectors.toSet()));
    }

    @Override
    public ProductDTO create(ProductDTO newUser) throws ConstraintsViolationException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ProductDTO findById(Long id) throws EntityNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void deleteById(Long id) throws InternalServerErrorExeption {
        // TODO Auto-generated method stub

    }

    @Override
    public ProductDTO alter(long id, ProductDTO clientDto)
            throws ConstraintsViolationException, EntityNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

}
