package com.rest.api.interview.service;

import com.rest.api.interview.dto.ProductDTO;

public interface IProductService extends IBasicService<ProductDTO> {

}
