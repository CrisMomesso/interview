package com.rest.api.interview.service.impl;

import java.util.List;

import com.rest.api.interview.dto.ClientDTO;
import com.rest.api.interview.exception.ConstraintsViolationException;
import com.rest.api.interview.exception.EntityNotFoundException;
import com.rest.api.interview.exception.InternalServerErrorExeption;
import com.rest.api.interview.mapper.ClientMapper;
import com.rest.api.interview.mapper.ProductMapper;
import com.rest.api.interview.model.Client;
import com.rest.api.interview.repository.IClientRepository;
import com.rest.api.interview.service.IClientService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements IClientService {

    private static final Logger LOG = LoggerFactory.getLogger(IClientService.class);

    @Autowired
    private IClientRepository clientRepo;

    @Override
    public List<ClientDTO> get(int page, int size) {
        return ClientMapper.makeUsersDTOList(clientRepo.findAll());
    }

    @Override
    public ClientDTO findByName(String name) throws EntityNotFoundException {
        Client client = clientRepo.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException("Client not found" + name));
        return ClientMapper.convertToDTO(client);
    }

    @Override
    public ClientDTO create(ClientDTO newUser) throws ConstraintsViolationException {
        Client model = clientRepo.save(ClientMapper.convertToEntity(newUser));
        try {
            model = clientRepo.save(model);
        } catch (DataIntegrityViolationException e) {
            LOG.warn("ConstraintsViolationException to create user: {}", model, e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return ClientMapper.convertToDTO(model);
    }

    @Override
    public ClientDTO findById(Long id) throws EntityNotFoundException {
        Client client = clientRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Client not found: " + id));
        return ClientMapper.convertToDTO(client);
    }

    @Override
    public void deleteById(Long id) throws InternalServerErrorExeption {
        try {
            clientRepo.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new InternalServerErrorExeption("Error when the system try to delete client");
        }
    }

    @Override
    public ClientDTO alter(long id, ClientDTO clientDto) throws ConstraintsViolationException, EntityNotFoundException {
        Client client = clientRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Client not found: " + id));
        client.setName(clientDto.getName());
        client.setEmail(clientDto.getEmail());
        client.setFavorite(ProductMapper.makeEntityList(clientDto.getFavorite()));
        return ClientMapper.convertToDTO(clientRepo.save(client));
    }
}