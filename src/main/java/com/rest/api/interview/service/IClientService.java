
package com.rest.api.interview.service;

import com.rest.api.interview.dto.ClientDTO;
import com.rest.api.interview.exception.EntityNotFoundException;

/**
 * IClient
 */
public interface IClientService extends IBasicService<ClientDTO> {
    ClientDTO findByName(String name) throws EntityNotFoundException;
}
