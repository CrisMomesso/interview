package com.rest.api.interview.mapper;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import com.rest.api.interview.dto.ProductDTO;
import com.rest.api.interview.model.Product;

public final class ProductMapper {
    private ProductMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static Product convertToEntity(ProductDTO dto) {
        final Product entity = new Product();
        entity.setId(dto.getId());
        entity.setPrice(dto.getPrice());
        entity.setBrand(dto.getBrand());
        entity.setImage(dto.getImage());
        entity.setReviewScore(dto.getReviewScore());
        entity.setTitle(dto.getTitle());
        return entity;
    }

    public static ProductDTO convertToDTO(Product entity) {
        final ProductDTO dto = new ProductDTO();
        dto.setId(entity.getId());
        dto.setPrice(entity.getPrice());
        dto.setBrand(entity.getBrand());
        dto.setImage(entity.getImage());
        dto.setReviewScore(entity.getReviewScore());
        dto.setTitle(entity.getTitle());
        return dto;
    }

    public static Set<ProductDTO> makeDTOList(Collection<Product> obj) {
        return obj.stream().map(ProductMapper::convertToDTO).collect(Collectors.toSet());
    }

    public static Set<Product> makeEntityList(Collection<ProductDTO> obj) {
        return obj.stream().map(ProductMapper::convertToEntity).collect(Collectors.toSet());
    }
}
