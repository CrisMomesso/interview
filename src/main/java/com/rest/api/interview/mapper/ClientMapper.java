package com.rest.api.interview.mapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.rest.api.interview.dto.ClientDTO;
import com.rest.api.interview.model.Client;

public final class ClientMapper {

    private ClientMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static Client convertToEntity(ClientDTO dto) {
        final Client entity = new Client();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setEmail(dto.getEmail());
        entity.setFavorite(ProductMapper.makeEntityList(dto.getFavorite()));
        return entity;
    }

    public static ClientDTO convertToDTO(Client entitty) {
        final ClientDTO dto = new ClientDTO();
        dto.setId(entitty.getId());
        dto.setName(entitty.getName());
        dto.setEmail(entitty.getEmail());
        dto.setFavorite(ProductMapper.makeDTOList(entitty.getFavorite()));
        return dto;
    }

    public static List<ClientDTO> makeUsersDTOList(Collection<Client> client) {
        return client.stream().map(ClientMapper::convertToDTO).collect(Collectors.toList());
    }

}