package com.rest.api.interview.dto;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductResponseDTO {
    private MetaDTO meta;
    private Set<ProductDTO> products;
}
