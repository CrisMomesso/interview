package com.rest.api.interview.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MetaDTO {
    private int pageNumber;
    private int pageSize =100;
}
