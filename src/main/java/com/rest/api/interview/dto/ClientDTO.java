package com.rest.api.interview.dto;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientDTO {
    private long id;
    private String name;
    private String email;
    private Set<ProductDTO> favorite;
}