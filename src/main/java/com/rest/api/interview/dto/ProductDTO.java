package com.rest.api.interview.dto;

import lombok.Data;

@Data
public class ProductDTO {
    private Long id;
    private double price;
    private String image;
    private String brand;
    private String title;
    private int reviewScore;
}
