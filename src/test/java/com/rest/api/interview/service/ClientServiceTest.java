package com.rest.api.interview.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import com.rest.api.interview.model.Client;
import com.rest.api.interview.model.Product;
import com.rest.api.interview.repository.IClientRepository;
import com.rest.api.interview.service.impl.ClientServiceImpl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ClientServiceTest {
    
    @InjectMocks
    ClientServiceImpl service;
    
    @Mock
    IClientRepository repo;
    
    @Test
    public void testGetClientById() throws Exception {
        Set<Product> favorite = new HashSet<>();
		Optional<Client> client = Optional.of(new Client(1l, "teste", "teste@teste.com.br", favorite));
        when(repo.findById(1L)).thenReturn(client);
        service.findById(1l);
		assertEquals(client.get().getId(), Long.valueOf(1L));
    }

}
