package com.rest.api.interview.controller;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.rest.api.interview.OAuthHelper;
import com.rest.api.interview.dto.ClientDTO;
import com.rest.api.interview.service.IClientService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class ClientControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private OAuthHelper authHelper;
	
	@MockBean
	private IClientService clientService;

	@Test
	
	public void testGetClientById() throws Exception {

        RequestPostProcessor bearerToken = authHelper.addBearerToken("cris");
			
		final ClientDTO dto = new ClientDTO(1l, "teste", "teste@teste.com.br", null);
		when(clientService.findById(1l)).thenReturn(dto);

		mockMvc.perform(get("/client/1").with(bearerToken)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.id", is(1)))
				.andExpect(jsonPath("$.name", is("teste")));
	}


}
